<?php
// Modify this constants with appropriate values for your own databas

/**
 * The server running the test database.
 */
const TEST_DB_HOST = 'localhost';

/**
 * The name of the test database.
 */
const TEST_DB_NAME = 'test';

/**
 * The name of the test book table.
 */
const TEST_TABLE_NAME = 'book';

/**
 * The database test user name for running tests.
 */
const TEST_DB_USER = 'runehj';

/**
 * The password to use for logging in as the database test user.
 */
const TEST_DB_PWD = 'IMT2571::admin';

/**
 * Name of the XML file defining the test database content.
 */
const FIXTURE_XML_FILE = 'UnitTestFixture.xml';

/**
 * The URL of the application root page (the page loading the controller).
 */
const TEST_BASE_URL = 'http://localhost/IMT2571/assignment1';
?>